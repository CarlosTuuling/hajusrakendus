<?php

namespace App\Http\Controllers;

use App\Models\Marker;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $markers = Marker::all();
        return Inertia::render('Map', [
            'markers' => $markers
        ]);
    }

    /**
     * Show the form for creating a new resource.
    */
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        Marker::create([
            'name' => $request->name,
            'description' => $request->description,
            'lat' => $request->lat,
            'lng' => $request->lng
        ]);



    }

    /**
     * Display the specified resource.
     */

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Marker $marker)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Marker $map)
    {
        $map->delete();
    }
}
