<?php

namespace App\Http\Controllers;

use App\Models\Monogatari;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


class MonogatariController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        return Inertia::render('Monogatari/Index', [
            'monogatari' => Monogatari::all(),
            'images' => Media::all()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Monogatari/Create', [
            'monogatari' => Monogatari::all(),
            'images' => Media::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:1000',
            'release_date' => 'required|string|max:255',
            'episode_count' => 'required|string|max:255',
        ]);
       $mono = Monogatari::create($validated);
        if ($images = $request->file('images')) {
            foreach ($images as $image) {
                $mono->addMedia($image)->toMediaCollection('images');
            }
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(Monogatari $monogatari)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Monogatari $monogatari)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Monogatari $monogatari)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Monogatari $monogatari)
    {
        //
    }
}
