<?php
use App\Http\Controllers\ChirpController;
use App\Http\Controllers\MarkerController;
use App\Models\Monogatari;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MonogatariController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentController;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\store;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/weather', function() {
    if (Cache::has('key') === false) {
        $response = Http::get('https://api.openweathermap.org/data/2.5/weather?lat=58.025842&lon=22.089151&appid=6023d17613eb16917bae34dc69fbc302&units=metric');
        Cache::put('key', $response->json(), now()->addMinutes(10));
    }
    return Inertia::render('Weather',
    ['weather' => Cache::get('key'),
    ]);
})->name('weather');

Route::resource('chirps', ChirpController::class)
    ->only(['index', 'store', 'update', 'destroy'])
    ->middleware(['auth', 'verified']);
    Route::resource('comments', CommentController::class)
    ->middleware(['auth', 'verified']);;
    Route::get('/monogatari', function (Request $request) {
        return $request->limit ? Monogatari::take($request->limit)->get() : Monogatari::all();
    });

    Route::get('/store', [store::class, 'index'])->name('store');
    Route::post('/addtocart', [store::class, 'addtocart'])->name('addcart');
    Route::get('/cart', [store::class, 'cart'])->name('cart');
    Route::post('/updatecart', [store::class, 'updatecart'])->name('updatecart');
    Route::post('/deleteitem', [store::class, 'deleteitem'])->name('deleteitem');
    Route::post('/subscribe', [store::class, 'subscribe'])->name('subscribe');
    Route::post('/success', [store::class, 'success'])->name('success');


    Route::get('/mailable', function () {
        $cart = session('cart');

        return new App\Mail\Paymentcomplete($cart);
    });


    Route::get('/products', [ProductsController::class, 'index'])->middleware(['auth'])->name('products');
    Route::get('/products/add', [ProductsController::class, 'create'])->middleware(['auth'])->name('products.add');
    Route::post('/products/store', [ProductsController::class, 'store'])->middleware(['auth'])->name('products.store');
    Route::get('/products/edit/{id}', [ProductsController::class, 'edit'])->middleware(['auth'])->name('products.edit');
    Route::post('/products/update', [ProductsController::class, 'update'])->middleware(['auth'])->name('products.update');
    Route::post('/products/delete', [ProductsController::class, 'destroy'])->middleware(['auth'])->name('products.delete');

    
    Route::get('/carlos', function() {

        if (Cache::has('Carlos') === false ) {
            $response = Http::get('https://kapsas.tak20tuuling.itmajakas.ee/api/monogatari');
            Cache::put('Carlos', $response->json(), now()->addMinutes(5));
        }
        return Inertia::render('Carlos', [
            'Carlos' => Cache::get('Carlos')
        ]);
    })->name('Carlos');
    Route::get('/kaarel', function() {

        if (Cache::has('kaarel') === false ) {
            $response = Http::get('https://hajus.tak20kallas.itmajakas.ee/api/games');
            Cache::put('kaarel', $response->json(), now()->addMinutes(5));
        }
        return Inertia::render('Kaarel', [
            'kaarel' => Cache::get('kaarel')
        ]);
    })->name('kaarel');


Route::controller(MarkerController::class)->name('map. ')->group(function() {
    Route::get('/map', 'index')->name('index');
    Route::post('/add-marker', 'store')->name('store');
    Route::put('/update-marker', 'update')->name('update');
    Route::delete('/destroy-marker', 'destroy')->name('destroy');
});
Route::resource('maps',MarkerController::class);
Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

});

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');
Route::resource('maps',MarkerController::class);
Route::resource('monogatari',MonogatariController::class);




Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
require __DIR__.'/auth.php';
