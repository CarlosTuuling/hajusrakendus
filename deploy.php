<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'hajus');
set('remote_user', 'virt101577'); //virt...
set('http_user', 'virt101577');
set('keep_releases', 2);

// Hosts
host('tak20tuuling.itmajakas.ee')
    ->setHostname('tak20tuuling.itmajakas.ee')
    ->set('http_user', 'virt101577')
    ->set('deploy_path', '~/domeenid/www.tak20tuuling.itmajakas.ee/kapsas')
    ->set('branch', 'master');

// Tasks
set('repository', 'https://gitlab.com/CarlosTuuling/hajusrakendus.git');
//Restart opcache
task('opcache:clear', function () {
    run('killall php81-cgi || true');
})->desc('Clear opcache');

task('build:node', function () {
    cd('{{release_path}}');
    run('npm i');
    run('npx vite build');
    run('rm -rf node_modules');
});
task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'build:node',
    'deploy:publish',
    'opcache:clear',
    'artisan:cache:clear'
]);
after('deploy:failed', 'deploy:unlock');
